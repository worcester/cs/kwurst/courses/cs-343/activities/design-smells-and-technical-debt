# Design Smells and Technical Debt

Poorly designed code causes problems with systems, making them hard to understand, modify, and maintain. Sometimes the problems were there from the start and sometimes they crept in as the code was modified. We can classify some of this poorly designed code as specific *design smells*. The difficulty this creates in maintaining and modifying code is often called *technical debt* and can be removed by *refactoring*.

## Content Learning Objectives

After completing this activity, students should be able to:

- Identify particular code design problems and name them as design smells.

## Process Skill Goals

During the activity, students should make progress toward:

- Matching technical uses of words to their technical definitions based on their common definitions. (Critical Thinking)

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Design Smells

If you are not familiar with some of the English words below, they have been linked to a dictionary definition.

- [Rigidity](https://www.britannica.com/dictionary/rigidity)
- [Fragility](https://www.britannica.com/dictionary/fragility)
- [Immobility](https://www.britannica.com/dictionary/immobility)
- [Viscosity](https://www.britannica.com/dictionary/viscosity)
- Needless [Complexity](https://www.britannica.com/dictionary/complexity)
- Needless [Repetition](https://www.britannica.com/dictionary/repetition)
- [Opacity](https://www.britannica.com/dictionary/opacity)

(Definitions from the [Britannica Dictionary](https://www.britannica.com/dictionary))

### Questions (15 min)

1. Match each definition below with one of the Design Smells above. Explain your reasoning.

    - The tendency of a program to break in many places when a single change is made. Often the new problems are in areas that have no conceptual relationship with the area that was changed.
    - The tendency of a module to be difficult to understand, possibly because it has evolved over time and no effort has been made to keep it clean and understandable.
    - When faced with a change, developers usually find more than one way to make the change. Some ways preserve the design, others do not (i.e. they are hacks.) When this design smell is high, it is easy to do the wrong thing, but hard to do the right thing.
    - The design contains parts that could be useful in other systems, but the effort and risk involved in separating those parts from the original system are too great.
    - When variants of the same code appear many places instead of being abstracted.
    - When a design contains elements that aren’t currently useful. This happens when developers anticipate changes that haven’t happened yet (and may never happen) and put facilities in the code to handle those changes.
    - The tendency for software to be difficult to change, even in simple ways. A single change causes a cascade of subsequent changes in dependent modules.

 Outside of class, read more about [code/design smells](https://gitlab.com/worcester/cs/kwurst/software-development-supplemental-materials#codedesign-smells).

 Also read about [technical debt](https://martinfowler.com/bliki/TechnicalDebt.html) and [refactoring](https://martinfowler.com/bliki/DefinitionOfRefactoring.html) on [Martin Fowler's blog](https://martinfowler.com/).

Copyright © 2023 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
